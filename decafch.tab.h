/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_DECAFCH_TAB_H_INCLUDED
# define YY_YY_DECAFCH_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    INT_CONST = 258,
    FLOAT_CONST = 259,
    STRING_CONST = 260,
    FLOAT_CONST_MANTISSA = 261,
    ID = 262,
    COMMENT = 263,
    SYN_ERR = 264,
    INT = 265,
    FLOAT = 266,
    IF = 267,
    ELSE = 268,
    WHILE = 269,
    FOR = 270,
    RETURN = 271,
    BREAK = 272,
    CONTINUE = 273,
    NULL_TOKEN = 274,
    TRUE = 275,
    FALSE = 276,
    THIS = 277,
    SUPER = 278,
    NEW = 279,
    CLASS = 280,
    EXTEND = 281,
    OPEN_BRACE = 282,
    CLOSE_BRACE = 283,
    SEMI_COLON = 284,
    STATIC = 285,
    PUBLIC = 286,
    PRIVATE = 287,
    BOOLEAN = 288,
    COMMA = 289,
    DOT = 290,
    OPEN_PARANTHESIS = 291,
    CLOSE_PARANTHESIS = 292,
    OPEN_SQR_BRACKET = 293,
    CLOSE_SQR_BRACKET = 294,
    VOID = 295,
    PLUS = 296,
    PLUS_PLUS = 297,
    MINUS = 298,
    MINUS_MINUS = 299,
    MULTIPLY = 300,
    DIVIDE = 301,
    NOT_EQUAL = 302,
    NOT = 303,
    OR = 304,
    AND = 305,
    EQUAL_EQUAL = 306,
    EQUAL = 307,
    LESS_EQUAL = 308,
    LESS = 309,
    GREATER_EQUAL = 310,
    GREATER = 311,
    EOF_TOKEN = 312,
    UMINUS = 313,
    UPLUS = 314,
    PREC_LOW = 315,
    PREC_HIGH = 316
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 22 "decafch.y" /* yacc.c:1909  */

	int ival;
	float fval;
	char *sval;
	char *cmtval;
	char *errval;
	char *strval;

#line 125 "decafch.tab.h" /* yacc.c:1909  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE yylval;
extern YYLTYPE yylloc;
int yyparse (void);

#endif /* !YY_YY_DECAFCH_TAB_H_INCLUDED  */
